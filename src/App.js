import './App.css';
import bussinessLogo from './images/logo-business.png'
import Button from './components/Button.jsx'
import Counter from './components/Counter.jsx'
import { useState } from 'react';

function App() {
  
  const [numClicks, setNumClicks] = useState(0); 
  
  const click = () => {
    setNumClicks(numClicks + 1);
  };
  
  const restartCounter = () => {
    setNumClicks(0);
  };
  
  return (
    <div className='App'>
      <div className='logo-bisiness-container'>
        <img 
          className='logo-business'
          src={bussinessLogo}
          alt='Logo Negocio'
        />
        <div className='container-principal'>
          <Counter 
            numClicks={numClicks}
          />
          <Button 
            text='Click'
            isButtonOfClick={true}
            handleClick={click}
          />
          <Button 
            text='Restart'
            isButtonOfClick={false}
            handleClick={restartCounter}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
