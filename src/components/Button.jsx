import '../stylesheets/Button.css'

function Button({ text, isButtonOfClick, handleClick }) {
  return (
    <button
      className={isButtonOfClick ? 'button-click' : 'button-restart'}
      onClick={handleClick}>
      {text}
    </button>
  );
}

export default Button;